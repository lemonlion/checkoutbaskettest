﻿using System.Collections.Generic;
using CheckoutBasketTest.Domain;
using CheckoutBasketTest.Domain.Models;

namespace CheckoutBasketTest.Data
{
    public interface ICheckoutBasketTestDataContext
    {
        IList<Product> Products { get; set; }
        IList<Customer> Customers { get; set; }
    }
}