﻿using StructureMap;

namespace CheckoutBasketTest.Data.DependencyInjection
{
    public class DataRegistry : Registry
    {
        public DataRegistry()
        {
            Scan(x =>
            {
                x.AssemblyContainingType<DataRegistry>();
                x.WithDefaultConventions();
            });
        }
    }
}
