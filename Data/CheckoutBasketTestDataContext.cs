﻿using CheckoutBasketTest.Domain;
using CheckoutBasketTest.Domain.Models;
using System;
using System.Collections;
using System.Collections.Generic;

namespace CheckoutBasketTest.Data
{
    public class CheckoutBasketTestDataContext : ICheckoutBasketTestDataContext
    {
        public IList<Product> Products { get; set; } = CheckoutBasketItems.Products;
        public IList<Customer> Customers { get; set; } = CheckoutBasketItems.Customers;
    }

    internal static class CheckoutBasketItems
    {
        public static IList<Product> Products { get; set; } = new List<Product>
        {
            new Product { Id = 1, Name = "IPad", Description = "Rectangular waste of money" },
            new Product { Id = 2, Name = "Chocolate", Description = "Something that will make you fat" }
        };

        public static IList<Customer> Customers { get; set; } = new List<Customer>
        {
            new Customer { Id = 1, Name = "John Smith", BasketItems = new List<BasketItem>() },
            new Customer { Id = 2, Name = "Brad Pitt", BasketItems = new List<BasketItem>() },
            new Customer { Id = 3, Name = "Jeniffer Aniston", BasketItems = new List<BasketItem>() }
        };
    }
}
