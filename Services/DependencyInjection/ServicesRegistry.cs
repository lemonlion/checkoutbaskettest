﻿using CheckoutBasketTest.Data.DependencyInjection;
using StructureMap;

namespace CheckoutBasketTest.Services.DependencyInjection
{
    public class ServicesRegistry : Registry
    {
        public ServicesRegistry()
        {
            IncludeRegistry<DataRegistry>();

            Scan(x =>
            {
                x.AssemblyContainingType<ServicesRegistry>();
                x.WithDefaultConventions();
            });
        }
    }
}
