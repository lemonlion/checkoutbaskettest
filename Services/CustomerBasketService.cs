﻿using CheckoutBasketTest.Data;
using System;
using System.Linq;
using CheckoutBasketTest.Domain.Models;
using System.Collections.Generic;
using CheckoutBasketTest.ServiceContract.ReadModels;

namespace CheckoutBasketTest.Services
{
    public class CustomerBasketService : ICustomerBasketService
    {
        private readonly ICheckoutBasketTestDataContext _checkoutBasketDataContext;

        public CustomerBasketService(ICheckoutBasketTestDataContext checkoutBasketDataContext)
        {
            _checkoutBasketDataContext = checkoutBasketDataContext;
        }

        public IEnumerable<BasketItemReadModel> GetAllBasketItems(int customerId)
        {
            var customer = _checkoutBasketDataContext.Customers.Single(x => x.Id == customerId);
            var basket = customer.BasketItems.Select(x => new BasketItemReadModel
            {
                Quantity = x.Quantity,
                Product = new ProductReadModel
                {
                    Id = x.Product.Id,
                    Name = x.Product.Name,
                    Description = x.Product.Description
                }
            });

            return basket;
        }

        public void AddItemToBasket(int customerId, int productId, int quantity)
        {
            var customer = _checkoutBasketDataContext.Customers.Single(x => x.Id == customerId);
            var basket = customer.BasketItems;
            var productInBasketAlready = basket.SingleOrDefault(x => x.Product.Id == productId);
            if (productInBasketAlready != null)
                productInBasketAlready.Quantity += quantity;
            else
            {
                var product = _checkoutBasketDataContext.Products.Single(x => x.Id == productId);
                basket.Add(new BasketItem { Customer = customer, Product = product, Quantity = quantity });
            }
        }

        public void ChangeQuantityOfItemFromBasket(int customerId, int productId, int quantity)
        {
            var customer = _checkoutBasketDataContext.Customers.Single(x => x.Id == customerId);
            var basket = customer.BasketItems;
            if(quantity == 0)
                basket.RemoveAll(x => x.Product.Id == productId);
            else
                basket.Single(x => x.Product.Id == productId).Quantity = quantity;
        }

        public void DeleteAllBasketItems(int customerId)
        {
            var customer = _checkoutBasketDataContext.Customers.Single(x => x.Id == customerId);
            customer.BasketItems = new List<BasketItem>();
        }
    }
}
