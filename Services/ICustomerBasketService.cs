﻿using System.Collections.Generic;
using CheckoutBasketTest.ServiceContract.ReadModels;

namespace CheckoutBasketTest.Services
{
    public interface ICustomerBasketService
    {
        void AddItemToBasket(int customerId, int productId, int quantity);
        void ChangeQuantityOfItemFromBasket(int customerId, int productId, int quantity);
        void DeleteAllBasketItems(int customerId);
        IEnumerable<BasketItemReadModel> GetAllBasketItems(int customerId);
    }
}