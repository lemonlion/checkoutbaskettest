using System;
using System.Collections.Generic;
using System.Linq;
using CheckoutBasketTest.Data;
using CheckoutBasketTest.Domain.Models;
using Moq;
using Xunit;
using Shouldly;

namespace CheckoutBasketTest.Services.Test.Unit
{
    public class CustomerBasketServiceTests
    {
        private readonly IList<Product> _testProducts = new List<Product>
        {
            new Product { Id = 1, Name = "IPad", Description = "Rectangular waste of money" },
            new Product { Id = 2, Name = "Chocolate", Description = "Something that will make you fat" }
        };

        private readonly IList<Customer> _testCustomers;
        private readonly Mock<ICheckoutBasketTestDataContext> _dataContextMock = new Mock<ICheckoutBasketTestDataContext>();
        private readonly CustomerBasketService _serviceUnderTest;

        public CustomerBasketServiceTests()
        {
            // Arrange
            _testCustomers = new List<Customer>
            {
                new Customer { Id = 1, Name = "John Smith", BasketItems = new List<BasketItem> {
                    new BasketItem { Product = _testProducts[0], Quantity = 1 },
                    new BasketItem { Product = _testProducts[1], Quantity = 2 } } },
                new Customer { Id = 2, Name = "Brad Pitt", BasketItems = new List<BasketItem>() },
                new Customer { Id = 3, Name = "Jeniffer Aniston", BasketItems = new List<BasketItem>() }
            };

            _dataContextMock.Setup(x => x.Products).Returns(_testProducts);
            _dataContextMock.Setup(x => x.Customers).Returns(_testCustomers);
            _serviceUnderTest = new CustomerBasketService(_dataContextMock.Object);
        }
        
        [Fact]
        public void Calling_GetAllBasketItems_Returns_Items()
        {
            // Arrange
            var customer = _testCustomers.First(x => x.BasketItems.Count > 1);
            var numberOfBasketItems = customer.BasketItems.Count;

            // Act
            var items = _serviceUnderTest.GetAllBasketItems(customer.Id);

            // Assert
            items.ShouldNotBeNull();
            items.ShouldNotBeEmpty();
            items.Count().ShouldBe(numberOfBasketItems);
        }

        [Fact]
        public void Calling_AddItemToBasket_Adds_New_Product_For_Non_Existent_Item()
        {
            // Arrange
            var customer = _testCustomers.Single(x => x.Id == 2);

            // Act
            _serviceUnderTest.AddItemToBasket(customer.Id, 1, 2);

            // Assert
            customer.BasketItems.Count.ShouldBe(1);
            customer.BasketItems.First().Product.Id.ShouldBe(1);
            customer.BasketItems.First().Quantity.ShouldBe(2);
        }

        [Fact]
        public void Calling_AddItemToBasket_Increases_Quantity_For_Existing_Item()
        {
            // Arrange
            var customer = _testCustomers.First(x => x.BasketItems.Count > 1);
            var originalQuantity = 2;
            var basketItemWithQuantity2 = customer.BasketItems.Single(x => x.Quantity == originalQuantity);
            var originalNumberOfBasketItems = customer.BasketItems.Count;
            var quantityToAdd = 3;

            // Act
            _serviceUnderTest.AddItemToBasket(customer.Id, basketItemWithQuantity2.Product.Id, quantityToAdd);

            // Assert
            customer.BasketItems.Count.ShouldBe(originalNumberOfBasketItems);
            basketItemWithQuantity2.Quantity.ShouldBe(originalQuantity + quantityToAdd);
        }

        [Fact]
        public void Calling_ChangeQuantityOfItemFromBasket_With_Non_Zero_Value_Changes_Quantity_To_That()
        {
            // Arrange
            var customer = _testCustomers.First(x => x.BasketItems.Count > 1);
            var basketItemWithQuantity2 = customer.BasketItems.Single(x => x.Quantity == 2);
            var originalNumberOfBasketItems = customer.BasketItems.Count;

            // Act
            _serviceUnderTest.ChangeQuantityOfItemFromBasket(customer.Id, basketItemWithQuantity2.Product.Id, 5);

            // Assert
            customer.BasketItems.Count.ShouldBe(originalNumberOfBasketItems);
            basketItemWithQuantity2.Quantity.ShouldBe(5);
        }
        
        [Fact]
        public void Calling_ChangeQuantityOfItemFromBasket_With_Zero_Value_Deletes_Item()
        {
            // Arrange
            var customer = _testCustomers.First(x => x.BasketItems.Count > 1);
            var basketItem = customer.BasketItems.First();
            var originalNumberOfBasketItems = customer.BasketItems.Count;

            // Act
            _serviceUnderTest.ChangeQuantityOfItemFromBasket(customer.Id, basketItem.Product.Id, 0);

            // Assert
            customer.BasketItems.Count.ShouldBe(originalNumberOfBasketItems - 1);
            customer.BasketItems.ShouldNotContain(x => x.Product.Id == basketItem.Product.Id);
        }

        [Fact]
        public void Calling_DeleteAllBasketItems_Deletes_All_Items()
        {
            // Arrange
            var customer = _testCustomers.First(x => x.BasketItems.Count > 1);

            // Act
            _serviceUnderTest.DeleteAllBasketItems(customer.Id);

            // Assert
            customer.BasketItems.ShouldBeEmpty();
        }
    }
}
