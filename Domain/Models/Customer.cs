﻿using System;
using System.Collections.Generic;

namespace CheckoutBasketTest.Domain.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public List<BasketItem> BasketItems { get; set; }
    }
}
