﻿using System;

namespace CheckoutBasketTest.Domain.Models
{
    public class BasketItem
    {
        public Customer Customer { get; set; }
        public Product Product { get; set; }
        public int Quantity { get; set; }
    }
}
