﻿using System;

namespace CheckoutBasketTest.ServiceContract.WriteModels
{
    public class AddBasketItemWriteModel
    {
        public int? Quantity { get; set; } = 1;
    }
}
