﻿using CheckoutBasketTest.ServiceContract.Enums;
using System;

namespace CheckoutBasketTest.ServiceContract.WriteModels
{
    public class PatchBasketItemWriteModel
    {
        public PatchOperation? Operation { get; set; }

        public int? Quantity { get; set; }
    }
}
