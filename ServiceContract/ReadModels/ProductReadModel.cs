﻿using System;

namespace CheckoutBasketTest.ServiceContract.ReadModels
{
    public class ProductReadModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
