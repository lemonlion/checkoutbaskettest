﻿using System;

namespace CheckoutBasketTest.ServiceContract.ReadModels
{
    public class BasketItemReadModel
    {
        public ProductReadModel Product { get; set; }
        public int Quantity { get; set; }
    }
}
