﻿using System;
using System.Runtime.Serialization;

namespace CheckoutBasketTest.Sdk
{
    [Serializable]
    internal class ApiCallFailureException : Exception
    {
        public int StatusCode { get; set; }
        public object Content { get; set; }
    }
}