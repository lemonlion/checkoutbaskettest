﻿using Newtonsoft.Json;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CheckoutBasketTest.Sdk.Services;

namespace CheckoutBasketTest.Sdk
{
    public class SdkService : ISdkService
    {
        // They keep changing their mind about whether to inject in HttpClient or IHttpClientFactory, seems the latest is to inject in HttpClient - https://docs.microsoft.com/en-us/dotnet/standard/microservices-architecture/implement-resilient-applications/use-httpclientfactory-to-implement-resilient-http-requests
        private readonly HttpClient _httpClient;

        public SdkService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        /// <summary>
        /// Sends a Get for the SDK and throws appropriate error on failure
        /// </summary>
        public async Task<T> SendGet<T>(string url, CancellationToken cancellationToken)
        {
            using (var request = new HttpRequestMessage(HttpMethod.Get, url))
            using (var response = await _httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false))
            {
                var stream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                    return DeserializeJsonFromStream<T>(stream);

                var content = await StreamToStringAsync(stream).ConfigureAwait(false);
                throw new ApiCallFailureException { StatusCode = (int)response.StatusCode, Content = content };
            }
        }

        /// <summary>
        /// Sends a Post/Patch/Delete for the SDK and throws appropriate error on failure
        /// </summary>
        public async Task SendPostPatchDelete<T>(HttpMethod httpMethod, string url, T writeModel, CancellationToken cancellationToken)
        {
            using (var request = new HttpRequestMessage(httpMethod, url))
            using (var httpContent = CreateHttpContent(writeModel))
            {
                request.Content = httpContent;
                using (var response = await _httpClient.SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken).ConfigureAwait(false))
                    response.EnsureSuccessStatusCode();
            }
        }

        /// <summary>
        /// Converts a stream to a string
        /// </summary>
        private async Task<string> StreamToStringAsync(Stream stream)
        {
            if (stream == null)
                return null;

            using (var streamReader = new StreamReader(stream))
                return await streamReader.ReadToEndAsync().ConfigureAwait(false);
        }

        /// <summary>
        /// Converts an object into an HttpContent
        /// </summary>
        public HttpContent CreateHttpContent(object content)
        {
            if (content == null)
                return null;

            HttpContent httpContent = null;

            using (var memoryStream = new MemoryStream())
            {
                SerializeJsonIntoStream(content, memoryStream);
                memoryStream.Seek(0, SeekOrigin.Begin);
                httpContent = new StreamContent(memoryStream);
            }
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return httpContent;
        }

        /// <summary>
        /// Deserializes a json stream into an object
        /// </summary>
        private T DeserializeJsonFromStream<T>(Stream stream)
        {
            if (stream == null || stream.CanRead == false)
                return default(T);

            using (var streamReader = new StreamReader(stream))
            using (var jsonTextReader = new JsonTextReader(streamReader))
                return new JsonSerializer().Deserialize<T>(jsonTextReader);
        }

        /// <summary>
        /// Serializes an object into a json stream
        /// </summary>
        private void SerializeJsonIntoStream(object value, Stream stream)
        {
            using (var streamWriter = new StreamWriter(stream, new UTF8Encoding(false), 1024, true))
            using (var jsonTextWriter = new JsonTextWriter(streamWriter) { Formatting = Formatting.None })
            {
                new JsonSerializer().Serialize(jsonTextWriter, value);
                jsonTextWriter.Flush();
            }
        }
    }
}
