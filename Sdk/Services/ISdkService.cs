﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace CheckoutBasketTest.Sdk.Services
{
    public interface ISdkService
    {
        Task<T> SendGet<T>(string url, CancellationToken cancellationToken);
        Task SendPostPatchDelete<T>(HttpMethod httpMethod, string url, T writeModel, CancellationToken cancellationToken);
        HttpContent CreateHttpContent(object content);
    }
}