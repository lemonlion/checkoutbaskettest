﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using CheckoutBasketTest.ServiceContract.ReadModels;
using CheckoutBasketTest.ServiceContract.WriteModels;

namespace CheckoutBasketTest.Sdk
{
    public interface ICheckoutBasketClient
    {
        Task AddBasketItem(int customerId, int productId, AddBasketItemWriteModel writeModel, CancellationToken cancellationToken);
        Task DeleteAllBasketItems(int customerId, CancellationToken cancellationToken);
        Task DeleteBasketItem(int customerId, int productId, CancellationToken cancellationToken);
        Task<IEnumerable<BasketItemReadModel>> GetAllBasketItems(int customerId, CancellationToken cancellationToken);
        Task UpdateBasketItem(int customerId, int productId, PatchBasketItemWriteModel writeModel, CancellationToken cancellationToken);
    }
}