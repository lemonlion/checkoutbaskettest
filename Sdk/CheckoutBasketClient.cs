﻿using CheckoutBasketTest.ServiceContract.ReadModels;
using CheckoutBasketTest.ServiceContract.WriteModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using CheckoutBasketTest.Sdk.Services;
using UrlCombineLib;

namespace CheckoutBasketTest.Sdk
{
    public class CheckoutBasketClient : ICheckoutBasketClient
    {
        private readonly ISdkService _sdkService;
        private readonly string _baseUrl;
        private const string basketItemsUrlComponent = "basket-items";

        public CheckoutBasketClient(ISdkService sdkService, CheckoutBasketClientSettings settings)
        {
            _sdkService = sdkService;
            _baseUrl = settings.BaseUrl;
        }

        /// <summary>
        /// Gets all the Basket Items for the given customer
        /// </summary>
        /// <param name="customerId">Customer id</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>A collection of Basket Items</returns>
        public async Task<IEnumerable<BasketItemReadModel>> GetAllBasketItems(int customerId, CancellationToken cancellationToken)
        {
            var url = UrlCombine.Combine(_baseUrl, $"{customerId}/{basketItemsUrlComponent}");
            return await _sdkService.SendGet<List<BasketItemReadModel>>(url, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Adds a Basket Item for the given customer
        /// </summary>
        /// <param name="customerId">Customer id</param>#
        /// <param name="productId">Product id</param>
        /// <param name="writeModel">Write model</param>
        /// <param name="cancellationToken">Cancellation token</param>
        public async Task AddBasketItem(int customerId, int productId, AddBasketItemWriteModel writeModel, CancellationToken cancellationToken)
        {
            var url = UrlCombine.Combine(_baseUrl, $"{customerId}/{basketItemsUrlComponent}/{productId}");
            await _sdkService.SendPostPatchDelete(HttpMethod.Post, url, writeModel, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Updates a Basket Item for the given customer
        /// </summary>
        /// <param name="customerId">Customer id</param>#
        /// <param name="productId">Product id</param>
        /// <param name="writeModel">Write model</param>
        /// <param name="cancellationToken">Cancellation token</param>
        public async Task UpdateBasketItem(int customerId, int productId, PatchBasketItemWriteModel writeModel, CancellationToken cancellationToken)
        {
            var url = UrlCombine.Combine(_baseUrl, $"{customerId}/{basketItemsUrlComponent}/{productId}");
            await _sdkService.SendPostPatchDelete(HttpMethod.Patch, url, writeModel, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Deletes a Basket Item for the given customer
        /// </summary>
        /// <param name="customerId">Customer id</param>#
        /// <param name="productId">Product id</param>
        /// <param name="cancellationToken">Cancellation token</param>
        public async Task DeleteBasketItem(int customerId, int productId, CancellationToken cancellationToken)
        {
            var url = UrlCombine.Combine(_baseUrl, $"{customerId}/{basketItemsUrlComponent}/{productId}");
            await _sdkService.SendPostPatchDelete<object>(HttpMethod.Delete, url, null, cancellationToken).ConfigureAwait(false);
        }

        /// <summary>
        /// Deletes alls Basket Items for the given customer
        /// </summary>
        /// <param name="customerId">Customer id</param>
        /// <param name="cancellationToken">Cancellation token</param>
        public async Task DeleteAllBasketItems(int customerId, CancellationToken cancellationToken)
        {
            var url = UrlCombine.Combine(_baseUrl, $"{customerId}/{basketItemsUrlComponent}");
            await _sdkService.SendPostPatchDelete<object>(HttpMethod.Delete, url, null, cancellationToken).ConfigureAwait(false);
        }
    }
}
