using System;
using System.Net.Http;
using Xunit;
using Moq;
using Moq.Protected;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.Collections.Generic;
using System.Reflection;
using CheckoutBasketTest.Sdk.Services;
using CheckoutBasketTest.ServiceContract.ReadModels;
using CheckoutBasketTest.ServiceContract.WriteModels;
using Newtonsoft.Json;
using Shouldly;

namespace CheckoutBasketTest.Sdk.Tests.Unit
{
    public class SdkServiceTests
    {
        private readonly string _url = "https://www.example.com/endpoint";
        private readonly Mock<HttpMessageHandler> _httpMessageHandlerMock = new Mock<HttpMessageHandler>();
        private readonly HttpClient _httpClient;
        private readonly ISdkService _serviceUnderTest;

        public SdkServiceTests()
        {
            // Arrange
            _httpClient = new HttpClient(_httpMessageHandlerMock.Object);
            _serviceUnderTest = new SdkService(_httpClient);
        }

        [Fact]
        public async Task On_Calling_SendGet_It_Should_Call_SendAsync_With_Correct_Parameters()
        { 
            // Arrange
            var expectedResponse = new List<TestClass> {new TestClass {Id = 1}};
            var expectedJsonResponse = JsonConvert.SerializeObject(expectedResponse);
            var responseMessage = new HttpResponseMessage { StatusCode = HttpStatusCode.OK, Content = new StringContent(expectedJsonResponse) };
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(x => x.RequestUri.ToString() == new Uri(_url).ToString()),
                ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(responseMessage)
                .Verifiable();

            // Act
            var response = await _serviceUnderTest.SendGet<List<TestClass>>(_url, new CancellationToken());

            // Assert
            JsonConvert.SerializeObject(response).ShouldBe(expectedJsonResponse);
        }

        [Fact]
        public async Task On_Calling_SendPostPatchDelete_It_Should_Call_SendAsync_With_Correct_Parameters()
        {
            // Arrange
            var requestBody = new TestClass { Id = 1 };
            var requestBodyContent =  _serviceUnderTest.CreateHttpContent(requestBody);
            var responseMessage = new HttpResponseMessage { StatusCode = HttpStatusCode.OK };
            _httpMessageHandlerMock.Protected().Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.Is<HttpRequestMessage>(x => x.RequestUri.ToString() == new Uri(_url).ToString() && ComputeLength(x.Content) == ComputeLength(requestBodyContent)),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(responseMessage)
                .Verifiable();

            // Act
            bool exceptionThrown = false;
            try
            {
                await _serviceUnderTest.SendPostPatchDelete(HttpMethod.Patch, _url, requestBody,new CancellationToken());
            }
            catch (Exception e)
            {
                exceptionThrown = true;
            }

            // Assert
            exceptionThrown.ShouldBeFalse();
        }

        public class TestClass
        {
            public int Id { get; set; }
        }

        private long ComputeLength(HttpContent content)
        {
            long length = 0;
            var methodInfo = typeof(HttpContent).GetMethod("TryComputeLength", BindingFlags.NonPublic | BindingFlags.Instance);
            methodInfo.Invoke(content, new object[] { length });
            return length;
        }
    }
}