using System;
using System.Net.Http;
using Xunit;
using Moq;
using System.Threading.Tasks;
using Moq.Protected;
using System.Net;
using System.Threading;
using System.Collections.Generic;
using CheckoutBasketTest.Sdk.Services;
using CheckoutBasketTest.ServiceContract.ReadModels;
using CheckoutBasketTest.ServiceContract.WriteModels;

namespace CheckoutBasketTest.Sdk.Tests.Unit
{
    public class CheckoutBasketClientTests
    {
        private readonly CheckoutBasketClientSettings _settings = new CheckoutBasketClientSettings { BaseUrl = "https://www.example.com/" };
        private readonly Mock<ISdkService> _sdkServiceMock = new Mock<ISdkService>();
        private readonly CheckoutBasketClient _serviceUnderTest;

        public CheckoutBasketClientTests()
        {
            // Arrange
            _serviceUnderTest = new CheckoutBasketClient(_sdkServiceMock.Object, _settings);
        }

        [Fact]
        public async Task On_Calling_GetAllBasketItems_It_Should_Call_SendGet_With_Correct_Parameters()
        {
            // Arrange
            var customerId = 1;
            var cancellationToken = new CancellationToken();

            // Act
            await _serviceUnderTest.GetAllBasketItems(customerId, cancellationToken);

            // Assert
            _sdkServiceMock.Verify(x => x.SendGet<List<BasketItemReadModel>>("https://www.example.com/1/basket-items", cancellationToken));
        }

        [Fact]
        public async Task On_Calling_AddBasketItem_It_Should_Call_SendPostPatchDelete_With_Correct_Parameters()
        {
            // Arrange
            var customerId = 1;
            var productId = 2;
            var writeModel = new AddBasketItemWriteModel { Quantity = 1 };
            var cancellationToken = new CancellationToken();

            // Act
            await _serviceUnderTest.AddBasketItem(customerId, productId, writeModel, cancellationToken);

            // Assert
            _sdkServiceMock.Verify(x => x.SendPostPatchDelete(HttpMethod.Post, "https://www.example.com/1/basket-items/2", writeModel, cancellationToken));
        }

        [Fact]
        public async Task On_Calling_UpdateBasketItem_It_Should_Call_SendPostPatchDelete_With_Correct_Parameters()
        {
            // Arrange
            var customerId = 1;
            var productId = 2;
            var writeModel = new PatchBasketItemWriteModel { Quantity = 1 };
            var cancellationToken = new CancellationToken();

            // Act
            await _serviceUnderTest.UpdateBasketItem(customerId, productId, writeModel, cancellationToken);

            // Assert
            _sdkServiceMock.Verify(x => x.SendPostPatchDelete(HttpMethod.Patch, "https://www.example.com/1/basket-items/2", writeModel, cancellationToken));
        }

        [Fact]
        public async Task On_Calling_DeleteBasketItem_It_Should_Call_SendPostPatchDelete_With_Correct_Parameters()
        {
            // Arrange
            var customerId = 1;
            var productId = 2;
            var cancellationToken = new CancellationToken();

            // Act
            await _serviceUnderTest.DeleteBasketItem(customerId, productId, cancellationToken);

            // Assert
            _sdkServiceMock.Verify(x => x.SendPostPatchDelete<object>(HttpMethod.Delete, "https://www.example.com/1/basket-items/2", null, cancellationToken));
        }

        [Fact]
        public async Task On_Calling_DeleteAllBasketItems_It_Should_Call_SendPostPatchDelete_With_Correct_Parameters()
        {
            // Arrange
            var customerId = 1;
            var cancellationToken = new CancellationToken();

            // Act
            await _serviceUnderTest.DeleteAllBasketItems(customerId, cancellationToken);

            // Assert
            _sdkServiceMock.Verify(x => x.SendPostPatchDelete<object>(HttpMethod.Delete, "https://www.example.com/1/basket-items", null, cancellationToken));
        }
    }
}