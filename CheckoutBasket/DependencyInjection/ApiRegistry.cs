﻿using CheckoutBasketTest.Services.DependencyInjection;
using StructureMap;

namespace CheckoutBasketTest.Api.DependencyInjection
{
    public class ApiRegistry : Registry
    {
        public ApiRegistry()
        {
            IncludeRegistry<ServicesRegistry>();

            Scan(x =>
            {
                x.AssemblyContainingType<ApiRegistry>();
                x.WithDefaultConventions();
            });
        }
    }
}
