﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CheckoutBasketTest.ServiceContract;
using CheckoutBasketTest.ServiceContract.Enums;
using CheckoutBasketTest.ServiceContract.ReadModels;
using CheckoutBasketTest.ServiceContract.WriteModels;
using CheckoutBasketTest.Services;
using Microsoft.AspNetCore.Mvc;

namespace CheckoutBasketTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerBasketService _customerBasketService;

        public CustomersController(ICustomerBasketService customerBasketService)
        {
            _customerBasketService = customerBasketService;
        }

        [HttpGet("{id}/basket-items")]
        public IEnumerable<BasketItemReadModel> GetAllBasketItems(int id)
        {
            var items = _customerBasketService.GetAllBasketItems(id);
            return items;
        }

        [HttpPost("{id}/basket-items/{productId}")]
        public void PostBasketItem(int id, int productId, [FromBody] AddBasketItemWriteModel model)
        {
            _customerBasketService.AddItemToBasket(id, productId, model.Quantity.Value);
        }
        
        [HttpPatch("{id}/basket-items/{productId}")]
        public void PatchBasketItem(int id, int productId, [FromBody] PatchBasketItemWriteModel model)
        {
            switch (model.Operation)
            {
                case PatchOperation.ChangeQuantity:
                    _customerBasketService.ChangeQuantityOfItemFromBasket(id, productId, model.Quantity.Value);
                    break;
            }
                
        }
        
        [HttpDelete("{id}/basket-items")]
        public void DeleteAllBasketItems(int id)
        {
            _customerBasketService.DeleteAllBasketItems(id);
        }

        [HttpDelete("{id}/basket-items/{productId}")]
        public void DeleteBasketItem(int id, int productId)
        {
            _customerBasketService.ChangeQuantityOfItemFromBasket(id, productId, 0);
        }
    }
}
